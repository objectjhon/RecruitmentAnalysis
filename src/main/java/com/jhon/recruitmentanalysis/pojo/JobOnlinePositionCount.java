package com.jhon.recruitmentanalysis.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class JobOnlinePositionCount {

    private String position;

    private Integer positionCount;

}
