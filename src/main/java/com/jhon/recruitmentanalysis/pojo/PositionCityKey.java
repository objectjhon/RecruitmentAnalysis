package com.jhon.recruitmentanalysis.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PositionCityKey {

    private String city;
    private Integer positionCount;

}
