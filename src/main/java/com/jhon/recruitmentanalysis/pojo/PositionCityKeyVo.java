package com.jhon.recruitmentanalysis.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PositionCityKeyVo {

    private String name;
    private Integer value;

}
