package com.jhon.recruitmentanalysis.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PositionCityKeyPo {

    private String name;
    private String value;

}
