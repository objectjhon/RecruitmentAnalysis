package com.jhon.recruitmentanalysis.pojo;

import lombok.Data;

@Data
public class CityCount {

    private String city;
    private Integer count;

}
